# FrontEnd Boilerplate

Techstack:
* Laravel-Mix (anstatt gulp.js)
* Docker
* tailwindCSS 2.x
* postCSS
* SASS

# Wie sieht der Techstack aus und worum geht's ganz grob?

tailwindCSS 1.x (alte Projekte) und tailwindCSS 2.x benötigen unterschiedliche node.js Versionen. Upgraden wir node.js, funktionieren unsere alten projekte nicht mehr. Selbiges gibt's auch bei gulp.js und anderen Tools. tailwindCSS ist hier nur das Beispiel.

Die Lösung für unterschiedliche Versionen: __Docker__

In Docker *Container* können wir unsere eigene Entwicklungsumgebung schaffen. In diesem Projekt beinhaltet der Container ein node.js Image in der Version 14, weshalb wir hier auch tailwindCSS 2.x benutzen können.

gulp.js ist kompliziert, Fehler anfällig und mittlerweile eher die 3. Wahl für Build-Tools. Wir verwenden hier `laravel-mix`, was ein Webpack Wrapper ist und wir ganz easy in der `webpack.mix.js` unsere Einstellungen vornehmen.

# Was beinhaltet diese Boilerplate?

- *tailwindCSS* mit dem neuen Just-In-Time (JIT) Compiler.
- CSS & JS *Cache-Busting* in der `mix-manifest.json`


# What is Docker?

__Ich empfehle euch dringendst die erste Stunde von diesem Video zu schauen.__ Hier wird erklärt, was Docker ist und es werden auch die Schritte der Installation gezeigt, die hier im Folgenden auch nochmal von mir gelistet werden.
[Watch this!](https://www.youtube.com/watch?v=d-PPOS-VsC8&ab_channel=Academind)

# Docker Installation

1. Install [Docker Desktop](https://hub.docker.com/editions/community/docker-ce-desktop-windows)
2. Enable _Hyper-V_ on Windows 10
   1. Open a __PowerShell__ (!) Terminal as administrator and run the
following command: `Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All`
   2. Optionally dive into the official docs for more information: https://docs.microsoft.com/en-us/virtualization/h
3. Enable Container features
   1. Open PowerShell as administrator and run the
following command: `Enable-WindowsOptionalFeature -Online -FeatureName containers –All`

__OPTIONAL: WSL installation__

WSL is a Linux installation inside of your Windows installation and that Linux installation will be used by Docker Desktop.

4. Install Windows Subsystem for Linux (WSL)
   1. You must first enable the "Windows Subsystem for Linux" optional feature before installing any Linux distributions on Windows. Open PowerShell as Administrator and run: `dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`
   2. Enable Virtual Machine feature. Open PowerShell as Administrator and run: `dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart`
   3. __Restart your machine to complete the WSL install and update to WSL 2.__
   4. Download the Linux kernel update package: [WSL2 Linux kernel update package for x64 machines](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)
   5. Set WSL 2 as your default version. Open PowerShell and run this command to set WSL 2 as the default version when installing a new Linux distribution: `wsl --set-default-version 2`
5. Finally, install your Linux distribution of choice. Follow this tutorial: [Install Ubuntu](https://docs.microsoft.com/en-us/windows/wsl/install-win10#step-6---install-your-linux-distribution-of-choice). I choose __Ubuntu 16.04 LTS__.
6. __Restart your machine__

Troubleshooting? Visit the [official docs](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

# Docker Settings

Starte Docker Desktop und gehe zu den Settings. 
- Unter *general* __deaktivieren__ wir *"Use the WSL 2 based engine"*. 
- Nun wird __Hyper-V__ anstatt __WSL__ benutzt. __WSL__ ist unter der aktuellen Windows Version extrem schlecht in der Performance! Nun, mit Hyper_V, ist die Performance ganz gut.
- __WSL__ benötigt *Windows 10 (OS build 20262 or higher)*, welches noch nicht offiziell zur Verfügung steht. 
- Mit dem kommenden Windows Update kann dann wieder __WSL__ in Docker Desktop aktiviert werden. Die Performance ist dann sehr viel besser als Hyper-V.

Mit der Aktivierung von __WSL__ müssen wir unsere Festplatte *mounten*. Dazu gibt's hier ein [Tutorial](https://docs.microsoft.com/de-de/windows/wsl/wsl2-mount-disk). __Achtung:__ Genau dieser Schritt ist der Grund, warum wir auf das Windows Update (build 20262 or higher) warten müssen. Ohne das Update können wir die Festplatte nicht mounten und der Befehl dazu (`wsl --mount <DiskPath>`) ist Windows nicht bekannt!


Bei __Hyper-V__ und __WSL__ geht's einfach gesagt um die Kommunikation zwischen unseren lokalen Dateien (Windows) und dem (virtuellen) Docker Container (Linux).

# Project setup

1. Add VS Code Extension *Docker* and *Docker Explorer*
2. Make sure *Docker Desktop* App is running
3. Clone bitbucket repo `docker-laravel-mix-stand-alone`
4. `docker-compose up -d --build npm` (build image and run npm container) 
   1. The container *YOUR-PROJECT-FOLDER-NAME_npm_1* is running
5. Switch to CLI inside the container
   1. `docker exec -it 498f0fcacf67 /bin/sh` - Starts the node CLI. *498f0fcacf67* is the Container ID. Type `docker ps` for a list of the containers and their id's.
6. Now we can run `npx mix` (Production) or `npx mix watch` (Development) for compiling etc.
   1. You may need to run `npm install` first.


# FAQ

## `npx mix` works, `npm mix` not! Why?

<mark style="padding:2px 4px; background-color:#ccc">__npx__</mark> - A tool for directly __executing__ Node packages without defining any scripts in the `package.json`.

And that's the case here! mix is not defined as a script. We access the package in Node_module directly.

<mark style="padding:2px 4px; background-color:#ccc">__npm__</mark> - npm by itself doesn't run any packages. If you want to run a package using npm, you must specify that package in your `package.json` and add that package in the script. The run the script using `npm run some-package`