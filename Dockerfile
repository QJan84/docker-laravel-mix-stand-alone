FROM node:14-alpine
WORKDIR /var/www/html 
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install
EXPOSE 80
COPY . /var/www/html 