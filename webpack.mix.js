let mix = require('laravel-mix');

mix.setPublicPath(`./`);

const postcss_import = require('postcss-import');
const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');

mix.js('src/js/main.js', 'dist/js')
  .version()

mix.sass('src/scss/main.scss', 'dist/css')
  .options({
      processCssUrls: false,
      postCss: [
        postcss_import(),
        tailwindcss('tailwind.config.js'),
        autoprefixer(),
        cssnano()
      ],
  });

mix.browserSync({
  proxy: 'localhost',
  open: false,
  files: ['src/**/*.*', 'templates/assets/**/*.*']
});
